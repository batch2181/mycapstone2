const express = require("express")
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// C R E A T E   P R O D U C T (A D M I N)
router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data.isAdmin)
    productController.createProduct(data).then(resultFromController => 
        res.send(resultFromController));
});

// G E T   A L L   P R O D U C T S
router.get("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// G E T   A C T I V E   P R O D U C T S
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});


// G E T   S P E C I F I C   P R O D U C T
router.get("/:productId", (req, res) => {
productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
});


// U P D A T E   P R O D U C T   I N F O
router.patch("/:productId/update", auth.verify, (req,res) => 
	{
		const newData = {
			product: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}
		productController.updateProduct(req.params.productId, newData).then(resultFromController => {
			res.send(resultFromController)
		});
});

// A R C H I V E   P R O D U C T
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const verifyAdmin = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(verifyAdmin.isAdmin)
	productController.archiveProduct(req.params.productId, verifyAdmin).then(resultFromController => {
		res.send(resultFromController)
	});
});













module.exports = router;