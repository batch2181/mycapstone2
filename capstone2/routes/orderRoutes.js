const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

// C R E A T E   O R D E R
router.post("/create", auth.verify, (req, res) => {
	const orderData = {
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(orderData.isAdmin)
    orderController.createOrder(orderData).then(resultFromController => 
        res.send(resultFromController));
});

// G E T   A L L   O R D E R S
router.get("/", auth.verify, (req, res) => {
	const verifyAdmin = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderController.getAllOrders(verifyAdmin).then(resultFromController => res.send(resultFromController))
});










module.exports = router;