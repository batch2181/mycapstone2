const Product = require("../models/product");
const mongoose = require("mongoose");


// C R E A T E   P R O D U C T (A D M I N)
module.exports.createProduct = (data) => {
    console.log(data)
    if(data.isAdmin) {
        let newProduct = new Product({
            productName: data.product.productName,
            description: data.product.description,
            price: data.product.price
        });
        return newProduct.save().then((product, error) => {
            if(error){
                return false
            }
            return newProduct
        })
    }
    let message = Promise.resolve('User must be ADMIN to access this.')

    return message.then((value) => {
        return {value}
    })
};

// G E T   A L L   P R O D U C T S
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
};

// G E T   A C T I V E   P R O D U C T S
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

// G E T   S P E C I F I C   P R O D U C T
module.exports.getProduct = (productId) => {
        return Product.findById(productId).then(result => {
            return result;
    })
};
    
// U P D A T E   P R O D U C T   I N F O
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin){
		return Product.findByIdAndUpdate(productId , 
			{
				productName: newData.product.productName,
				description: newData.product.description,
				price: newData.product.price
			}).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return `Successfully updated product: ${updatedProduct.productName} details!`
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
};


// A R C H I V E   P R O D U C T
module.exports.archiveProduct = (productId, verifyAdmin) => {
    if(verifyAdmin.isAdmin){
        return Product.findByIdAndUpdate(productId, {
            isActive: false
        }
    )
        .then((archivedProduct, error) => {
            if(error){
                return false
            } 
            return {
                message: "Archived product successfully!"
            }
        })
    } else {
        let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
    }
};
