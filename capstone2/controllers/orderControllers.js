const Order = require("../models/order");
const mongoose = require("mongoose");

// C R E A T E   O R D E R
module.exports.createOrder = (orderData) => {
    console.log(orderData)
    if(orderData.isAdmin == false) {
        let newOrder = new Order({
            userId: orderData.order.userId,
            product: [{
            productId: orderData.order.productId,
            quantity: orderData.order.quantity}],
            totalAmount: orderData.order.totalAmount
        });
        return newOrder.save().then((order, error) => {
            if(error){
                return false
            }
            return orderData.order
        })
    }
    let message = Promise.resolve('ADMIN cannot access this feature.')

    return message.then((value) => {
        return {value}
    })
};

// G E T   A L L   O R D E R S
module.exports.getAllOrders = (verifyAdmin) => {
    if(verifyAdmin.isAdmin){
        return Order.find({}).then(result => {
            return result;
        })
    } else {
        let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
    }
};
