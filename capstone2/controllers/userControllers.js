const User = require("../models/user");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt")
const auth = require("../auth.js");

// R E G I S T R A T I O N
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        mobileNo: reqBody.mobileNo,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) => {
    if(error){
        return false;
    } else {
        return `Hi ${newUser.firstName}, your account has been successfully created!`;
    }
})
};


// L O G I N
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return "Account does not exist.";
		}
		else{
            console.log(reqBody.password)
            console.log(result.password)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            console.log(isPasswordCorrect)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return "Incorrect password.";
			}
		}
	})
};

// G E T   U S E R   D E T A I L S
module.exports.getUserProfile = (userId) => {
    return User.findById(userId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
            result.password = ""
			return result;
		}
	})

};


// G E T   A L L   U S E R S
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
};
